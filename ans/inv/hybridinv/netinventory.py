#!/usr/bin/python3
"""Alta3 Research || rzfeeser@alta3.com
   Dynamic Inventory script"""

## for accepting arguments from user at CLI
import argparse

## required for working with JSON (pystd. library)
import json

def main():
    # this is what we will ultimately return
    inventory = {}

    # Called with `--list`
    if args.list:
        # inventory is result of calling example_inventory()
        inventory = example_inventory()
    # Called with `--host [hostname]`
    elif args.host:
        # Not implemented, since we return _meta info `--list`
        # Let's put API request logic here...
        inventory = empty_inventory()
    # If no groups or vars are present, return an empty inventory.
    else:
        inventory = empty_inventory()

    # print the result of inventory
    print(json.dumps(inventory))  # from the json library use the
                                  # DUMPString function, dumps()

# Example inventory for testing.
def example_inventory():
    return {
        'group': {
            'hosts': ['sw-1', 'sw-2'],
            'vars': {
                'ansible_ssh_pass': 'alta3',
                'ansible_connection': 'network_cli',
                'ansible_network_os': 'eos'
            }
        },
        '_meta': {
            'hostvars': {
                'sw-1': {
                    'ansible_user': 'admin'
                },
                'sw-2': {
                    'ansible_user': 'admin'
                }
            }
        }
    }

# Empty inventory for testing.
def empty_inventory():
    return {'_meta': {'hostvars': {}}}

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--list', action = 'store_true')
    parser.add_argument('--host', action = 'store')
    args = parser.parse_args()
    main()

